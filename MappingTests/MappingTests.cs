﻿using NHibernate;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using Mappings;
using FluentNHibernate.Testing;
using VideoStore.Utilities;
using System.Collections;

namespace MappingTests
{
    public class MappingTests
    {
        private ISession _session;
        private Movie starWars;
        private Movie hoosiers;
                
        [SetUp]
        public void CreateSession ()
        {
            _session = SessionFactory.CreateSessionFactory().GetCurrentSession();
            _session.CreateSQLQuery("delete from videostore.Area").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.Customer").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.Video").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.Store").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.ZipCode").ExecuteUpdate();

            starWars = _session.Load<Movie>("tt0076759");
            var title = starWars.Title;

            hoosiers = _session.Load<Movie>("tt0091217");
            title = hoosiers.Title;
        }

        [Test]
        public void ZipCodeMappingIsCorrect()
        {
            new PersistenceSpecification<ZipCode>(_session)
                .CheckProperty(z => z.Code, "49464")
                .CheckProperty(z => z.City, "Zeeland")
                .CheckProperty(z => z.State, "Michigan");
        }

        [Test]
        public void AreaMappingIsCorrect()
        {
            var zipCodes = new List<ZipCode>
            {
                new ZipCode {
                    Code = "49423",
                    City = "Holland",
                    State = "Michigan"
                },
                new ZipCode
                {
                    Code = "48444",
                    City = "Imlay City",
                    State = "Michigan"
                }
            };

            new PersistenceSpecification<Area>(_session, new DateEqualityComparer())
                .CheckProperty(a => a.Name, "Zeeland")                
                .CheckBag(a => a.ZipCodes, zipCodes)
                .VerifyTheMappings();
        }
                       
        [Test]
        public void CustomerMappingIsCorrect()
        {
            var zeeland = new ZipCode
            {
                Code = "49464",
                City = "Zeeland",
                State = "Michigan"
            };

            var stores = new List<Store>
            {
                new Store
                {
                    StoreName = "Store 2",
                    StreetAddress = "1234 Winterwood Lane",
                    PhoneNumber = "616-748-9715",
                    ZipCode = zeeland
                },
                new Store
                {
                    StoreName = "Store 1",
                    StreetAddress = "1260 Winterwood Lane",
                    PhoneNumber = "616-123-4567",
                    ZipCode = zeeland
                },
            };

            new PersistenceSpecification<Customer>(_session)
                .CheckProperty(c => c.Name, new Name()
                {
                    Title = "Dr.",
                    First = "Ryan",
                    Middle = "L",
                    Last = "McFall",
                    Suffix = "Sr"
                })
                .CheckProperty(c => c.EmailAddress, "mcfall@hope.edu")
                .CheckProperty(c => c.StreetAddress, "27 Graves Place VWF 220")
                .CheckProperty(c => c.Password, "Abc123$!")
                .CheckProperty(c => c.Phone, "616-395-7952")
                .CheckReference(c => c.ZipCode,
                    new ZipCode()
                    {
                        Code = "49423",
                        City = "Holland",
                        State = "Michigan"
                    }
                )   
                .CheckInverseList(
                    c => c.PreferredStores, stores, 
                    (cust, store) => cust.AddPreferredStore(store)
                )
                .VerifyTheMappings();
        }

        [Test]
        public void StoreMappingIsCorrect()
        {
            var videos = new List<Video>()
            {
                new Video
                {
                    Movie = starWars,
                    NewArrival = false,
                    PurchaseDate = DateTime.Now
                },
                new Video
                {
                    Movie = hoosiers,
                    NewArrival = true, 
                    PurchaseDate = DateTime.Now.AddDays(1)
                }
            };

            new PersistenceSpecification<Store>(_session)
                .CheckProperty(s => s.StoreName, "Blockbusted")
                .CheckProperty(s => s.StreetAddress, "1234 Broke Street")
                .CheckProperty(s => s.PhoneNumber, "616-666-0000")
                .CheckReference(s => s.ZipCode, 
                    new ZipCode() {
                        Code = "49423",
                        City = "Holland",
                        State = "Michigan"
                    }
                )
                .CheckInverseList( s => s.Videos, videos, (s, v) => s.AddVideo(v))
                .VerifyTheMappings();
        }

        [Test]
        public void VideoMappingIsCorrect()
        {
            new PersistenceSpecification<Video>(_session, new DateEqualityComparer())
                .CheckProperty(v => v.NewArrival, true)
                .CheckProperty(v => v.PurchaseDate, DateTime.Now)
                .CheckReference(v => v.Store, 
                    new Store()
                    {
                        StoreName = "Last Store",
                        StreetAddress = "1234 Broke St",
                        PhoneNumber = "616-666-0000",
                        ZipCode = new ZipCode()
                        {
                            Code = "49423",
                            City = "Holland",
                            State = "Michigan"
                        }
                    }
                )
                .CheckReference(v => v.Movie, starWars)
                .VerifyTheMappings();
        }

        [Test]
        public void MovieMappingIsCorrect()
        {            
            Assert.AreEqual("tt0076759", starWars.TitleId);
            Assert.AreEqual("Star Wars: Episode IV - A New Hope", starWars.Title);
            Assert.AreEqual("Star Wars", starWars.OriginalTitle);
            Assert.AreEqual(1977, starWars.Year);
            Assert.AreEqual(121, starWars.RunningTimeInMinutes);
            Assert.AreEqual("PG", starWars.Rating);
        }
    }
}
