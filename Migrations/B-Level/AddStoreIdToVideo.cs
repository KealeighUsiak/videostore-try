﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(11)]
    public class AddStoreIdToVideo : Migration
    {
        public override void Down()
        {
            Delete.Column("Store_Id")
                .FromTable("Video")
                .InSchema("videostore");
        }

        public override void Up()
        {
            Alter.Table("Video")
                .InSchema("videostore")
                .AddColumn("Store_Id").AsInt32().NotNullable();
        }
    }
}
