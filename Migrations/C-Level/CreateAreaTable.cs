﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(2)]
    public class AreaMigration : Migration
    {
        public override void Down()
        {
            Delete.Table("Area").InSchema("videostore");
        }

        public override void Up()
        {
            Create.Table("Area").InSchema("videostore")
                .WithColumn("Id").AsInt32().Identity().PrimaryKey()
                .WithColumn("Name").AsString(255).Unique().NotNullable();

        }
    }
}
