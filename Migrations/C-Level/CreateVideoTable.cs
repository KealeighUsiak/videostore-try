﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(5)]
    public class CreateVideoTable : Migration
    {
        public override void Down()
        {
            Delete.Table("Video")
                .InSchema("videostore");
        }

        public override void Up()
        {
            Create.Table("Video")
                .InSchema("videostore")
                .WithColumn("Id").AsInt32().Identity().PrimaryKey()
                .WithColumn("PurchaseDate").AsDateTime().NotNullable()
                .WithColumn("NewArrival").AsBoolean().NotNullable();
        }
    }
}
