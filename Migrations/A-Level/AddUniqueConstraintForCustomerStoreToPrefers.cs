﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(22)]
    public class AddUniqueConstraintForCustomerStoreToPrefers : Migration
    {
        public override void Down()
        {
            Delete.UniqueConstraint("CustomerStoreUnique")
                .FromTable("Prefers")
                .InSchema("videostore");
        }

        public override void Up()
        {
            Create.UniqueConstraint("CustomerStoreUnique")
                .OnTable("Prefers")
                .WithSchema("videostore")
                .Columns("Customer_Id", "Store_Id");
        }
    }
}
