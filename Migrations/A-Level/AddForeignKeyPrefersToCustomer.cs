﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(20)]
    public class AddForeignKeyPrefersToCustomer : Migration
    {
        public override void Down()
        {
            Delete.ForeignKey("PrefersToCustomer")
                .OnTable("Prefers")
                .InSchema("videostore");
        }

        public override void Up()
        {
            Create.ForeignKey("PrefersToCustomer")
                .FromTable("Prefers")
                .InSchema("videostore")
                .ForeignColumn("Customer_Id")
                .ToTable("Customer")
                .InSchema("videostore")
                .PrimaryColumn("Id")
                .OnDeleteOrUpdate(System.Data.Rule.Cascade);
        }
    }
}
