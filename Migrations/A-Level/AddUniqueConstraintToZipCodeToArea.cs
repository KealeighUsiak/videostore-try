﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(18)]
    public class AddUniqueConstraintToZipCodeToArea : Migration
    {
        public override void Down()
        {
            Delete.UniqueConstraint("AreaZipCodeUnique")
                .FromTable("ZipCodeToArea")
                .InSchema("videostore");
        }

        public override void Up()
        {
            Create.UniqueConstraint("AreaZipCodeUnique")
                .OnTable("ZipCodeToArea")
                .WithSchema("videostore")
                .Columns("ZipCode_Id", "Area_Id");
        }
    }
}
