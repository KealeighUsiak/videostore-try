﻿using FluentNHibernate.Mapping;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mappings
{
    public class MovieMap : ClassMap<Movie>
    {
        public MovieMap()
        {
            Table("Movies");
            Schema("imdb");
            Id(m => m.TitleId).GeneratedBy.Assigned();
            Map(m => m.Title);
            Map(m => m.OriginalTitle);
            Map(m => m.RunningTimeInMinutes);
            Map(m => m.Rating, "MPAARating");
            Map(m => m.Year, "YearReleased");            
        }
    }
}
